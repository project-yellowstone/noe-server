FROM node:10-alpine

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

# building the code for production
RUN npm install --only=production

# Bundle app source
COPY . .

HEALTHCHECK --interval=12s --timeout=12s --retries=3 CMD [ "PORT=8080 node /healthcheck.js" ]

ENV NODE_ENV production
ENV PORT=8080

EXPOSE 8080
CMD [ "npm", "start" ]

# Ensure that the programm will start as non root user
USER node
