# noe-server

> CESI MSIA18 Noe Project - Server awesome part

## About

This project uses [Feathers](http://feathersjs.com). An open source web framework for building modern real-time applications.

## Getting Started

Getting up and running is as easy as 1, 2, 3, 4.

1. Make sure you have [NodeJS](https://nodejs.org/) and [npm](https://www.npmjs.com/) installed.
2. Install your dependencies

    ```bash
    cd path/to/noe-server
    npm install
    ```

3. Init tour MongoDB database

    ```bash
    # Create a container from the mongo image,  run is as a daemon (-d), expose the port 27017 (-p), set it to auto start (--restart) and with mongo authentication (--auth)
    # Image used is https://hub.docker.com/_/mongo/
    docker pull mongo
    docker run --name mongodb_dev --restart=always -d -p 27017:27017 mongo mongod --auth

    # Using the mongo "localhost exception" (https://docs.mongodb.org/v3.0/core/security-users/#localhost-exception) add a root user

    # Bash into the container
    sudo docker exec -i -t mongodb_dev bash

    # Connect to local mongo
    mongo

    # Create the first admin user with root rights on all databases
    use admin
    db.createUser({user:"root",pwd:"root",roles:[{role:"root",db:"admin"}]})

    # Create the dev user for noe_server with root rights only on noe_server database
    use noe_server
    db.createUser({ user: "root", pwd: "root", roles:[{ role: "readWrite", db: "noe_server" }, { role: "dbAdmin", db: "noe_server" }])

    # Exit the mongo shell
    exit
    # Exit the container
    exit

    # Now you can connect with the admin user
    # Remember to use --authenticationDatabase "admin"
    mongo -u "root" -p "root" localhost --authenticationDatabase "admin"

    # Test the noe_server profile
    mongo -u "root" -p "root" localhost --authenticationDatabase "noe_server"
    ```

4. Start the app

    ```bash
    npm start
    ```

## Testing

Simply run `npm test` and all your tests in the `test/` directory will be run.

## Scaffolding

Feathers has a powerful command line interface. Here are a few things it can do:

```
$ npm install -g @feathersjs/cli          # Install Feathers CLI

$ feathers generate service               # Generate a new Service
$ feathers generate hook                  # Generate a new Hook
$ feathers help                           # Show all commands
```

## Help

For more information on all the things you can do with Feathers visit [docs.feathersjs.com](http://docs.feathersjs.com).

## Changelog

__0.1.0__

- Initial release

## License

Copyright (c) 2018

Licensed under the [MIT license](LICENSE).
