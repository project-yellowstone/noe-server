const assert = require('assert');
const app = require('../../src/app');

describe("'authentication' service", () => {
  it('registered the service', () => {
    const service = app.service('authentication');

    assert.ok(service, 'Registered the service');
  });
});
