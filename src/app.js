// external libs
const Sentry = require('@sentry/node');
const path = require('path');
const favicon = require('serve-favicon');
const compress = require('compression');
const helmet = require('helmet');
const cors = require('cors');
// npm package

// feathers libs
const feathers = require('@feathersjs/feathers');
const configuration = require('@feathersjs/configuration');
const express = require('@feathersjs/express');
const socketio = require('@feathersjs/socketio');
const npmPackage = require('../package.json');

const middleware = require('./middleware');
const services = require('./services');
const appHooks = require('./app.hooks');
const channels = require('./channels');

const mongoose = require('./mongoose');

const app = express(feathers());

// Load app configuration
app.configure(configuration());

// Define app env & versions
app.set('appName', npmPackage.name);
app.set('appDescription', npmPackage.description);
app.set(
  'env',
  process.env.NODE_ENV === 'production' ? 'production' : 'development',
);
app.set('appVersion', `${npmPackage.name}@${npmPackage.version}`);

// Init logger
const logging = require('./logging')(app);
app.logger = logging.logger;
app.configure(logging.proto);

// Enable security, CORS, compression, favicon and body parsing
app.use(helmet());
app.use(cors());
app.use(compress());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(favicon(path.join(app.get('public'), 'favicon.ico')));
// Host the public folder
app.use('/', express.static(app.get('public')));

// Set up Plugins and providers
app.configure(express.rest());
app.configure(
  socketio({
    transports: ['websocket'],
  }),
);

app.configure(mongoose);

// The request handler must be the first middleware on the app
app.use(Sentry.Handlers.requestHandler());

// Configure other middleware (see `middleware/index.js`)
app.configure(middleware);
// Set up our services (see `services/index.js`)
app.configure(services);
// Set up event channels (see channels.js)
app.configure(channels);

// The error handler must be before any other error middleware
app.use(Sentry.Handlers.errorHandler());

// Configure a middleware for 404s and the error handler
app.use(express.notFound());
app.use(express.errorHandler({ logger: logging }));

// register global app hooks
app.hooks(appHooks);

module.exports = app;
