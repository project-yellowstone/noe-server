const util = require('util');

module.exports = () => context => {
  context.app.debug(
    `${context.type} app.service('${context.path}').${context.method}()`,
  );

  if (typeof context.toJSON === 'function') {
    context.app.debug('Hook Context', util.inspect(context, { colors: false }));
  }

  if (context.error && !context.result) {
    context.app.error(context.error.stack);
  }
};
