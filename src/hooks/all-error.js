const errors = require('@feathersjs/errors');

module.exports = () => context => {
  if (context.error) {
    const { error } = context;
    if (!error.code) {
      const newError = new errors.GeneralError('server error');
      // eslint-disable-next-line no-param-reassign
      context.error = newError;
      return context;
    }
    if (error.code === 404 || process.env.NODE_ENV === 'production') {
      error.stack = null;
    }
  }
  return context;
};
