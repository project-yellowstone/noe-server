require('dotenv').config();

const Healthcheck = require('./healthcheck');

const app = require('./app');

// error handling, as up as possible
process.on('unhandledRejection', (reason, p) => {
  app.error('Unhandled Rejection at: Promise ', p, reason);
});

const port = app.get('port');
const server = app.listen(port);

// output server availability
server.once('listening', () =>
  app.info(
    'Feathers application started on http://%s:%d',
    app.get('host'),
    port,
  ),
);

// init healthcheck
const healthcheck = Healthcheck(app, server);
healthcheck.createService();
