module.exports = app => {
  const mongooseClient = app.get('mongooseClient');
  const { Schema } = mongooseClient;
  const users = new Schema(
    {
      email: { type: String, required: true, unique: true, lowercase: true },
      password: { type: String, required: true },
      person: {
        type: Schema.Types.ObjectId,
        ref: 'persons',
        required: true,
        unique: true,
      },
      permissions: [
        {
          type: Schema.Types.ObjectId,
          ref: 'iam',
        },
      ],
    },
    {
      timestamps: true,
    },
  );

  return mongooseClient.model('users', users);
};
