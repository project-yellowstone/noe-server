module.exports = app => {
  const mongooseClient = app.get('mongooseClient');
  const { Schema } = mongooseClient;
  const persons = new Schema(
    {
      birthday: Date,
      phone: String,
      avatar: String,
      firstname: { type: String, required: true },
      lastname: { type: String, required: true },
      status: [
        {
          type: { type: String, required: true },
          start: { type: Date, required: true },
          end: { type: Date },
        },
      ],
      address: {
        city: { type: String, required: true },
        country: { type: String, required: true },
        postal: String,
        line_1: { type: String, required: true },
        line_2: String,
        geo: {
          lat: { type: Number, required: true },
          long: { type: Number, required: true },
        },
      },
    },
    {
      timestamps: true,
    },
  );

  return mongooseClient.model('persons', persons);
};
