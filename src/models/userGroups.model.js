module.exports = app => {
  const mongooseClient = app.get('mongooseClient');
  const { Schema } = mongooseClient;
  const userGroups = new Schema(
    {
      name: { type: String, unique: true, required: true },
      permissions: [
        {
          type: Schema.Types.ObjectId,
          ref: 'iam',
          required: true,
        },
      ],
      users: [
        {
          type: Schema.Types.ObjectId,
          ref: 'users',
        },
      ],
    },
    {
      timestamps: true,
    },
  );

  return mongooseClient.model('userGroups', userGroups);
};
