module.exports = app => {
  const mongooseClient = app.get('mongooseClient');
  const { Schema } = mongooseClient;
  const iam = new Schema(
    {
      name: { type: String, unique: true, required: true },
      description: { type: String },
      permissions: [{ type: String, required: true }],
    },
    {
      timestamps: true,
    },
  );

  return mongooseClient.model('iam', iam);
};
