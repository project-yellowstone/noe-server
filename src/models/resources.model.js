module.exports = app => {
  const mongooseClient = app.get('mongooseClient');
  const { Schema } = mongooseClient;
  const resources = new Schema(
    {
      species: {
        type: Schema.Types.ObjectId,
        ref: 'phylogenetic',
      },
      type: { type: String, required: true },
      status: [
        {
          date: { type: Date, required: true },
          type: { type: String, required: true },
          person: {
            type: Schema.Types.ObjectId,
            ref: 'users',
            required: true,
          },
        },
      ],
    },
    {
      timestamps: true,
    },
  );

  return mongooseClient.model('resources', resources);
};
