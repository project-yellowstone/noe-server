module.exports = app => {
  const mongooseClient = app.get('mongooseClient');
  const { Schema } = mongooseClient;
  const phylogenetic = new Schema(
    {
      name: { type: String, unique: true, required: true },
      parent: {
        type: Schema.Types.ObjectId,
        ref: 'phylogenetic',
      },
      status: [
        {
          type: { type: String, required: true },
          start: { type: Date, required: true },
          end: { type: Date },
        },
      ],
    },
    {
      timestamps: true,
    },
  );

  return mongooseClient.model('phylogenetic', phylogenetic);
};
