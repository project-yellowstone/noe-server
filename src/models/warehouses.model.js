module.exports = app => {
  const mongooseClient = app.get('mongooseClient');
  const { Schema } = mongooseClient;
  const warehouses = new Schema(
    {
      status: [
        {
          type: { type: String, required: true },
          start: { type: Date, required: true },
          end: Date,
        },
      ],
      address: {
        city: { type: String, required: true },
        country: { type: String, required: true },
        postal: String,
        line_1: { type: String, required: true },
        line_2: String,
        geo: {
          lat: { type: Number, required: true },
          long: { type: Number, required: true },
        },
      },
      description: String,
      rooms: [
        {
          constraints: {
            humidity: { type: Number, required: true },
            lumosity: { type: Number, required: true },
            temperature: { type: Number, required: true },
          },
          lockers: [
            {
              type: { type: String, required: true },
              things: [
                {
                  type: Schema.Types.ObjectId,
                  ref: 'ressources',
                },
              ],
            },
          ],
        },
      ],
    },
    {
      timestamps: true,
    },
  );

  return mongooseClient.model('warehouses', warehouses);
};
