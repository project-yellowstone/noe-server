module.exports = app => {
  const mongooseClient = app.get('mongooseClient');
  const { Schema } = mongooseClient;
  const project = new Schema(
    {
      name: { type: String, unique: true, required: true },
      writings: [
        {
          description: String,
          date: { type: Date, required: true },
          content: { type: String, required: true },
          person: {
            type: Schema.Types.ObjectId,
            ref: 'users',
            required: true,
          },
        },
      ],
      status: [
        {
          type: { type: String, required: true },
          metadata: {
            persons: {
              from: {
                type: Schema.Types.ObjectId,
                ref: 'users',
                required: true,
              },
              to: {
                type: Schema.Types.ObjectId,
                ref: 'users',
              },
            },
            description: String,
            start: { type: Date, required: true },
            end: Date,
          },
        },
      ],
      ressources: [
        {
          type: Schema.Types.ObjectId,
          ref: 'ressources',
        },
      ],
    },
    {
      timestamps: true,
    },
  );

  return mongooseClient.model('project', project);
};
