const Proto = require('uberproto');
const { createLogger, format, transports } = require('winston');
const Sentry = require('@sentry/node');
const { SentryTransport } = require('winston-node-sentry');

module.exports = app => {
  const modules = [];

  const { combine, timestamp, splat, json } = format;

  // Configure the default logger. Domplete documentation: https://github.com/winstonjs/winston
  const logger = createLogger({
    level: app.get('logLevel'),
    format: combine(splat(), timestamp(), json()),
    transports: [
      new transports.Console(),
      new SentryTransport({
        level: 'error',
        sentryOpts: {
          dsn: app.get('sentry'),
          sentry: Sentry,
          environment: app.get('env'),
          release: app.get('appVersion'),
        },
      }),
    ],
  });

  modules.logger = logger;

  modules.proto = () => {
    if (typeof logger === 'function') {
      app.use(logger);
    } else if (typeof logger !== 'undefined') {
      app.set('logger', logger);
    }

    Proto.mixin(
      {
        bindedLogger: logger,

        log() {
          if (
            this.bindedLogger &&
            typeof this.bindedLogger.log === 'function'
          ) {
            // eslint-disable-next-line prefer-rest-params
            return this.bindedLogger.log(...arguments);
          }

          // eslint-disable-next-line no-console, prefer-rest-params
          return console.log('LOG: ', arguments);
        },

        info() {
          if (
            this.bindedLogger &&
            typeof this.bindedLogger.info === 'function'
          ) {
            // eslint-disable-next-line prefer-rest-params
            const argumentsNew = Array.prototype.slice.call(arguments);
            argumentsNew.unshift('info');
            return this.bindedLogger.log(...argumentsNew);
          }

          // eslint-disable-next-line no-console, prefer-rest-params
          return console.info('INFO: ', arguments);
        },

        warn() {
          if (
            this.bindedLogger &&
            typeof this.bindedLogger.warn === 'function'
          ) {
            // eslint-disable-next-line prefer-rest-params
            const argumentsNew = Array.prototype.slice.call(arguments);
            argumentsNew.unshift('warn');
            return this.bindedLogger.log(...argumentsNew);
          }

          // eslint-disable-next-line no-console, prefer-rest-params
          return console.warn('WARNING: ', arguments);
        },

        error() {
          if (
            this.bindedLogger &&
            typeof this.bindedLogger.error === 'function'
          ) {
            // eslint-disable-next-line prefer-rest-params
            const argumentsNew = Array.prototype.slice.call(arguments);
            argumentsNew.unshift('error');
            return this.bindedLogger.log(...argumentsNew);
          }

          // eslint-disable-next-line no-console, prefer-rest-params
          return console.error('ERROR: ', arguments);
        },

        debug() {
          if (
            this.bindedLogger &&
            typeof this.bindedLogger.debug === 'function'
          ) {
            // eslint-disable-next-line prefer-rest-params
            const argumentsNew = Array.prototype.slice.call(arguments);
            argumentsNew.unshift('debug');
            return this.bindedLogger.log(...argumentsNew);
          }

          // eslint-disable-next-line no-console, prefer-rest-params
          return console.error('DEBUG: ', arguments);
        },
      },
      app,
    );
  };

  return modules;
};
