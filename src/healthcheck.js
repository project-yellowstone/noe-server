const { createTerminus } = require('@godaddy/terminus');

module.exports = (app, server) => {
  const modules = {};

  modules.createService = () => {
    createTerminus(server, {
      signal: 'SIGINT',
      healthChecks: {
        '/healthcheck': () => {
          app.info('Test app health...');

          const mongooseOk =
            app.get('mongooseClient').connection.readyState === 1;
          if (!mongooseOk) throw new Error('Mongoose client is KO');
        },
      },
      onSignal: () => {
        app.info('Exit the app...');
      },
      logger: app.logger.error,
    });
  };

  return modules;
};
