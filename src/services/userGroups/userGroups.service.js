// Initializes the `userGroups` service on path `/userGroups`
const createService = require('feathers-mongoose');
const m2s = require('mongoose-to-swagger');
const createModel = require('../../models/userGroups.model');
const hooks = require('./userGroups.hooks');

module.exports = app => {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    Model,
    paginate,
    lean: false,
  };

  // Init the service
  const events = createService(options);
  events.docs = {
    definitions: {
      userGroups: m2s(Model),
      'userGroups list': {
        type: 'array',
        items: { $ref: '#/definitions/userGroups' },
      },
    },
  };

  app.use('/userGroups', events);

  // Get the service so that we can register hooks
  const service = app.service('userGroups');

  service.hooks(hooks);
};
