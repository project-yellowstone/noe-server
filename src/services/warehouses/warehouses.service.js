// Initializes the `warehouses` service on path `/warehouses`
const createService = require('feathers-mongoose');
const m2s = require('mongoose-to-swagger');
const createModel = require('../../models/warehouses.model');
const hooks = require('./warehouses.hooks');

module.exports = app => {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    Model,
    paginate,
    lean: false,
  };

  // Init the service
  const events = createService(options);
  events.docs = {
    definitions: {
      warehouses: m2s(Model),
      'warehouses list': {
        type: 'array',
        items: { $ref: '#/definitions/warehouses' },
      },
    },
  };

  app.use('/warehouses', events);

  // Get the service so that we can register hooks
  const service = app.service('warehouses');

  service.hooks(hooks);
};
