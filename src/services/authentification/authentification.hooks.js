const { authenticate } = require('@feathersjs/authentication').hooks;
const { protect } = require('@feathersjs/authentication-local').hooks;

module.exports = config => ({
  before: {
    all: [],
    find: [],
    get: [],
    create: [authenticate(config.strategies)],
    update: [],
    patch: [],
    remove: [authenticate('jwt')],
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [
      async context => {
        // eslint-disable-next-line no-param-reassign
        context.result.userData = await context.app
          .service('users')
          .get(context.params.payload.usersId); // eslint-disable-line no-underscore-dangle, prettier/prettier
        return context;
      },
      protect('userData.password'),
    ],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
});
