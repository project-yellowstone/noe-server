// Initializes the `authentification` service on path `/authentification`
const authentication = require('@feathersjs/authentication');
const jwt = require('@feathersjs/authentication-jwt');
const local = require('@feathersjs/authentication-local');

const authenticationHook = require('./authentification.hooks');

module.exports = app => {
  const config = app.get('authentication');
  const hooks = authenticationHook(config);

  // Set up authentification layer
  app.configure(authentication(config));
  app.configure(jwt());
  app.configure(local());

  // Get the service so that we can register hooks
  const service = app.service('authentication');

  service.hooks(hooks);
};
