const authentification = require('./authentification/authentification.service');
const iam = require('./iam/iam.service');
const persons = require('./persons/persons.service');
const phylogenetic = require('./phylogenetic/phylogenetic.service');
const projects = require('./projects/projects.service');
const resources = require('./resources/resources.service');
const userGroups = require('./userGroups/userGroups.service');
const users = require('./users/users.service');
const warehouses = require('./warehouses/warehouses.service');

module.exports = app => {
  app.configure(authentification);
  app.configure(iam);
  app.configure(persons);
  app.configure(phylogenetic);
  app.configure(projects);
  app.configure(resources);
  app.configure(userGroups);
  app.configure(users);
  app.configure(warehouses);
};
