const { authenticate } = require('@feathersjs/authentication').hooks;
const {
  hashPassword,
  protect,
} = require('@feathersjs/authentication-local').hooks;
const errors = require('@feathersjs/errors');
const Validator = require('fastest-validator');

const v = new Validator();

const fullSchema = {
  $$strict: true,
  email: { type: 'email', empty: false },
  password: { type: 'string', empty: false },
  person: { type: 'uuid', version: 4 },
  permissions: {
    type: 'array',
    items: { type: 'uuid', version: 4 },
  },
};
const fullSchemaCheck = v.compile(fullSchema);
function fullSchemaCheckHook(context) {
  const checkRs = fullSchemaCheck(context.data);
  if (!checkRs) {
    // eslint-disable-next-line no-param-reassign
    context.error = new errors.BadRequest('Validation failed', {
      validation: checkRs,
    });
  }
  return context;
}

module.exports = {
  before: {
    all: [authenticate('jwt')],
    find: [],
    get: [],
    create: [hashPassword(), fullSchemaCheckHook],
    update: [hashPassword(), fullSchemaCheckHook],
    patch: [hashPassword()],
    remove: [],
  },

  after: {
    all: [
      // Make sure the password field is never sent to the client. Always must be the last hook!
      protect('password'),
    ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};
