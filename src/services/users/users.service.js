// Initializes the `users` service on path `/users`
const createService = require('feathers-mongoose');
const m2s = require('mongoose-to-swagger');
const createModel = require('../../models/users.model');
const hooks = require('./users.hooks');

module.exports = app => {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    Model,
    paginate,
    lean: false,
  };

  // Init the service
  const events = createService(options);
  events.docs = {
    definitions: {
      users: m2s(Model),
      'users list': {
        type: 'array',
        items: { $ref: '#/definitions/users' },
      },
    },
  };

  app.use('/users', events);

  // Get the service so that we can register hooks
  const service = app.service('users');

  service.hooks(hooks);
};
