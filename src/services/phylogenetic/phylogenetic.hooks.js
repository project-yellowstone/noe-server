const { authenticate } = require('@feathersjs/authentication').hooks;
const errors = require('@feathersjs/errors');
const Validator = require('fastest-validator');

const v = new Validator();

const fullSchema = {
  $$strict: true,
  name: { type: 'string', empty: false },
  status: {
    type: 'array',
    items: {
      type: 'object',
      strict: true,
      props: {
        type: {
          type: 'enum',
          values: ['toSave', 'disappearance', 'inDanger'],
        },
        start: { type: 'date', convert: true },
        end: { type: 'date', convert: true, optional: true },
      },
    },
  },
  parent: {
    type: 'uuid',
    version: 4,
    optional: true,
  },
};
const fullSchemaCheck = v.compile(fullSchema);
function fullSchemaCheckHook(context) {
  const checkRs = fullSchemaCheck(context.data);
  if (!checkRs) {
    // eslint-disable-next-line no-param-reassign
    context.error = new errors.BadRequest('Validation failed', {
      validation: checkRs,
    });
  }
  return context;
}

module.exports = {
  before: {
    all: [authenticate('jwt')],
    find: [],
    get: [],
    create: [fullSchemaCheckHook],
    update: [fullSchemaCheckHook],
    patch: [],
    remove: [],
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};
