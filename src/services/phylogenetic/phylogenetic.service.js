// Initializes the `phylogenetic` service on path `/phylogenetic`
const createService = require('feathers-mongoose');
const m2s = require('mongoose-to-swagger');
const createModel = require('../../models/phylogenetic.model');
const hooks = require('./phylogenetic.hooks');

module.exports = app => {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    Model,
    paginate,
    lean: false,
    whitelist: ['$populate'],
  };

  // Init the service
  const events = createService(options);
  events.docs = {
    definitions: {
      phylogenetic: m2s(Model),
      'phylogenetic list': {
        type: 'array',
        items: { $ref: '#/definitions/phylogenetic' },
      },
    },
  };

  app.use('/phylogenetic', events);

  // Get the service so that we can register hooks
  const service = app.service('phylogenetic');

  service.hooks(hooks);
};
