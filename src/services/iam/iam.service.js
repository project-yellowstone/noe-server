// Initializes the `iam` service on path `/iam`
const createService = require('feathers-mongoose');
const m2s = require('mongoose-to-swagger');
const createModel = require('../../models/iam.model');
const hooks = require('./iam.hooks');

module.exports = app => {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    Model,
    paginate,
    lean: false,
  };

  // Init the service
  const events = createService(options);
  events.docs = {
    definitions: {
      iam: m2s(Model),
      'iam list': {
        type: 'array',
        items: { $ref: '#/definitions/iam' },
      },
    },
  };

  app.use('/iam', events);

  // Get the service so that we can register hooks
  const service = app.service('iam');

  service.hooks(hooks);
};
