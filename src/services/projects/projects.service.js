// Initializes the `projects` service on path `/projects`
const createService = require('feathers-mongoose');
const m2s = require('mongoose-to-swagger');
const createModel = require('../../models/projects.model');
const hooks = require('./projects.hooks');

module.exports = app => {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    Model,
    paginate,
    lean: false,
  };

  // Init the service
  const events = createService(options);
  events.docs = {
    definitions: {
      projects: m2s(Model),
      'projects list': {
        type: 'array',
        items: { $ref: '#/definitions/projects' },
      },
    },
  };

  app.use('/projects', events);

  // Get the service so that we can register hooks
  const service = app.service('projects');

  service.hooks(hooks);
};
