const { authenticate } = require('@feathersjs/authentication').hooks;
const {
  hashPassword,
  protect,
} = require('@feathersjs/authentication-local').hooks;
const errors = require('@feathersjs/errors');
const Validator = require('fastest-validator');

const v = new Validator();

const fullSchema = {
  $$strict: true,
  name: { type: 'string', empty: false },
  writings: {
    type: 'array',
    items: {
      type: 'object',
      strict: true,
      props: {
        description: { type: 'string', empty: false, optional: true },
        date: { type: 'date', convert: true },
        content: { type: 'string', empty: false },
        person: { type: 'uuid', version: 4 },
      },
    },
  },
  status: {
    type: 'array',
    items: {
      type: 'object',
      strict: true,
      props: {
        type: {
          type: 'enum',
          values: [
            'dataCollection',
            'testimony',
            'personProjectOwner',
            'needExpertise',
            'nogo',
            'go',
            'personFieldMissionary',
            'personNarrator',
            'personInternalTracking',
          ],
        },
        metadata: {
          type: 'object',
          strict: true,
          props: {
            persons: {
              type: 'object',
              strict: true,
              props: {
                from: { type: 'uuid', version: 4 },
                to: { type: 'uuid', version: 4, optional: true },
              },
            },
            description: { type: 'string', empty: false, optional: true },
            start: { type: 'date', convert: true },
            end: { type: 'date', convert: true, optional: true },
          },
        },
      },
    },
  },
  ressources: {
    type: 'array',
    items: { type: 'uuid', version: 4 },
  },
};
const fullSchemaCheck = v.compile(fullSchema);
function fullSchemaCheckHook(context) {
  const checkRs = fullSchemaCheck(context.data);
  if (!checkRs) {
    // eslint-disable-next-line no-param-reassign
    context.error = new errors.BadRequest('Validation failed', {
      validation: checkRs,
    });
  }
  return context;
}

module.exports = {
  before: {
    all: [authenticate('jwt')],
    find: [],
    get: [],
    create: [hashPassword(), fullSchemaCheckHook],
    update: [hashPassword(), fullSchemaCheckHook],
    patch: [hashPassword()],
    remove: [],
  },

  after: {
    all: [
      // Make sure the password field is never sent to the client. Always must be the last hook!
      protect('password'),
    ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};
