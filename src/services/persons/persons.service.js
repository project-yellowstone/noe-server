// Initializes the `persons` service on path `/persons`
const createService = require('feathers-mongoose');
const m2s = require('mongoose-to-swagger');
const createModel = require('../../models/persons.model');
const hooks = require('./persons.hooks');

module.exports = app => {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    Model,
    paginate,
    lean: false,
  };

  // Init the service
  const events = createService(options);
  events.docs = {
    definitions: {
      persons: m2s(Model),
      'persons list': {
        type: 'array',
        items: { $ref: '#/definitions/persons' },
      },
    },
  };

  app.use('/persons', events);

  // Get the service so that we can register hooks
  const service = app.service('persons');

  service.hooks(hooks);
};
