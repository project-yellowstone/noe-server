const { authenticate } = require('@feathersjs/authentication').hooks;
const errors = require('@feathersjs/errors');
const Validator = require('fastest-validator');

const v = new Validator();

const fullSchema = {
  $$strict: true,
  birthday: 'date',
  phone: { type: 'string', empty: false, optional: true },
  avatar: { type: 'string', empty: false, optional: true },
  firstname: { type: 'string', empty: false },
  lastname: { type: 'string', empty: false },
  status: {
    type: 'object',
    strict: true,
    props: {
      type: { type: 'enum', values: ['employee', 'member', 'external'] },
      start: { type: 'date', convert: true },
      end: { type: 'date', convert: true, optional: true },
    },
  },
  address: {
    type: 'object',
    strict: true,
    props: {
      city: 'string',
      country: 'string',
      postal: { type: 'string', empty: false, optional: true },
      line_1: 'string',
      line_2: { type: 'string', empty: false, optional: true },
      geo: {
        type: 'object',
        strict: true,
        props: {
          lat: 'number',
          long: 'number',
        },
      },
    },
  },
};
const fullSchemaCheck = v.compile(fullSchema);
function fullSchemaCheckHook(context) {
  const checkRs = fullSchemaCheck(context.data);
  if (!checkRs) {
    // eslint-disable-next-line no-param-reassign
    context.error = new errors.BadRequest('Validation failed', {
      validation: checkRs,
    });
  }
  return context;
}

module.exports = {
  before: {
    all: [authenticate('jwt')],
    find: [],
    get: [],
    create: [fullSchemaCheckHook],
    update: [fullSchemaCheckHook],
    patch: [],
    remove: [],
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};
