// Initializes the `resources` service on path `/resources`
const createService = require('feathers-mongoose');
const m2s = require('mongoose-to-swagger');
const createModel = require('../../models/resources.model');
const hooks = require('./resources.hooks');

module.exports = app => {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    Model,
    paginate,
    lean: false,
    whitelist: ['$populate'],
  };

  // Init the service
  const events = createService(options);
  events.docs = {
    definitions: {
      resources: m2s(Model),
      'resources list': {
        type: 'array',
        items: { $ref: '#/definitions/resources' },
      },
    },
  };

  app.use('/resources', events);

  // Get the service so that we can register hooks
  const service = app.service('resources');

  service.hooks(hooks);
};
