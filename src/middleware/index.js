const swagger = require('feathers-swagger');

module.exports = app => {
  registerSwagger(app);
};

// Configure Swagger documentation generation
function registerSwagger(app) {
  if (app.get('env') !== 'production') {
    app.configure(
      swagger({
        uiIndex: true,
        docsPath: '/docs',
        info: {
          title: app.get('appName'),
          description: app.get('appDescription'),
        },
        securityDefinitions: {
          bearerAuth: {
            type: 'apiKey',
            name: 'Authorization',
            in: 'header',
          },
        },
      }),
    );
  }
}
