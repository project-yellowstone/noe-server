const request = require('request');
const requestPort = process.env.PORT ? process.env.PORT : 80;

logInfo(`Request on port ${requestPort}...`);

request(
  `http://localhost:${requestPort}/healthcheck`,
  { json: true },
  (err, res, body) => {
    if (err) {
      logError(err.message);
      end(false);
    }

    logDebug(JSON.stringify(body));

    if (body.status && body.status === 'ok') {
      end(true);
    }

    end(false);
  },
);

function end(isValid) {
  if (!isValid) {
    logInfo('ERROR');
    process.exit(1);
  }

  logInfo('OK');
  process.exit(0);
}

function logError(str) {
  // eslint-disable-next-line no-console
  console.error(`ERROR: ${str}`);
}

function logInfo(str) {
  // eslint-disable-next-line no-console
  console.log(`INFO: ${str}`);
}

function logDebug(str) {
  // eslint-disable-next-line no-console
  console.log(`DEBUG: ${str}`);
}
